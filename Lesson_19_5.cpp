#include <iostream>

using namespace std;

class Animal
{		
public:
	Animal() {}


	virtual void Voice()
	{
		cout << " up " << "\n";
		
	}
};

class Dog : public Animal
{
public:
	void Voice() override
	{
		cout << "Wooof!! Woof!!" << '\n';
		
	}
};

class Cat : public Animal
{
public:
	void Voice() override
	{
		cout << "Meow! Meeoow!" << '\n';
		
	}
};

class Owl : public Animal
{
public:
	void Voice() override
	{
		cout << "YyYYYyY" << '\n';
		
	}
};

int main()

{
	
	Dog D;
	Cat C;
	Owl O;

	const int x = 3;

	/*Animal* q =  &D;
	Animal* q =  &C;
	Animal* q =  &O;*/

	Animal* Voices[x] = { &D, &C, &O};

	for (int i = 0; i < x; i++)
	{
		Voices[i]->Voice();
	}
	



}